Installation
============

- enable the module
- set permissions for ec_ship
- visit "admin/ecsettings/products/types" and select "Shippable Product"
- select "add feature"
- select "Shipping Dimensions" and click "Add Product Feature"
- you can now add weight, length, height, and width to your product nodes

Keep in mind that you still need a shipping provider that is compatible with this module and ec4.