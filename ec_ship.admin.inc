<?php


/**
 * @file
 * Administration settings for ec_shipping
 *
 */

/**
 * 
 * Provide administration settings 
 * 
 */
function ec_ship_admin_settings() {

 if ( $options = ec_ship_get_shipping_methods()) {
   $form= array();
   $form['defaults'] = array(
      '#title' => t("Default Settings"),
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
   );

   $form['defaults']['ec_ship_default_method'] = array(
      '#type' => 'select',
      '#options' => ec_ship_get_shipping_methods(),
      '#default_value' => variable_get('ec_ship_default_method', ''),
      '#title' => t('Shipping Method'),
   );
 }
 else {
   drupal_set_message("You need to enable a shipping provider module.");
   return;
 }
 return system_settings_form($form);
}