<?php


/**
 * @file
 * Features implemenations for ec_shipping
 *
 */

/**
 * 
 * Implementation of hook_feature_attributes().
 * 
 */
function ec_ship_feature_dimensions_attributes($node) {
  return array('dimensions' => TRUE);
}

/*
 * 
 * 
 * 
 */
function ec_ship_update(&$node) {
  drupal_write_record('ec_product', $node, array('vid'));
}

/*
 * 
 * 
 * 
 */
function ec_ship_load(&$node) {
  $data = db_fetch_object(db_query('SELECT height,width,length,weight FROM {ec_product} AS data WHERE data.vid = %d', $node->vid));
  $node->height = $data->height;
  $node->width = $data->width;
  $node->length = $data->length;
  $node->weight = $data->weight;
  return;
}
