<?php



/**
 * @file
 * Theme functions for ec_shipping
 */
/**
 * 
 * Themes the address form elements on the checkout review page
 * 
 */
function theme_ec_ship_review_form($form) {
  $title = drupal_render($form['title']);
  drupal_add_js(drupal_get_path('module', 'ec_customer') .'/ec_ship.checkout.js', 'module');
  drupal_add_css(drupal_get_path('module', 'ec_customer') .'/ec_ship.css');
  return theme('box', $title, drupal_render($form));
}