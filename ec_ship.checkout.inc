<?php


/**
 * @file
 * Implementation of checkout API for ecommerce
 *
 */

/**
 * 
 * Implementation of hook_checkout_init().
 * 
 */
function ec_ship_checkout_init(&$txn) {
  $weight = 0;
  if ($txn->shippable && empty($txn->shipping)) {
    foreach ($txn->items as $item) {
      if ( isset($item->shippable) and $item->shippable) {
        $weight = floatval($item->weight)+floatval($weight);
      }
    }
    $txn->shipping['description'] = t('Shipping');
    $txn->shipping['totalweight'] = $weight;
    $txn->shipping['method'] = variable_get('ec_ship_default_method', '');
    $txn->shipping['method_name'] = ec_ship_get_shipping_method_name($txn);
    $txn->shipping['provider'] = ec_ship_get_shipping_provider($txn);
    $txn->shipping['provider_name'] = ec_ship_get_shipping_provider_name($txn);

#   if (empty($txn->shipping['method'])) {
#      $txn->shipping['method'] = variable_get('ec_ship_default_method', '');
#   }
  }
}

/**
 * 
 * Implementation of hook_checkout_calculate().
 * 
 */
function ec_ship_checkout_calculate(&$form_state) {
  $txn =& $form_state['txn'];

  if ($txn->shippable )  {
#    $txn->shipping['description'] = t('Shipping');
#    $txn->shipping['method'] = $form_state['txn']->shipping['method'];
#    $txn->shipping['method_name'] = ec_ship_get_shipping_method_name($txn);
    
#    $txn->shipping['provider'] = 'ec_ups';
#    $txn->shipping['provider_name'] = ec_ship_get_shipping_provider_name($txn);
    if ((isset($txn->address)) && (!empty($txn->address) )) {
      $txn->shipping['price'] = ec_ship_get_shipping_cost($txn);
    }
  }
}

/**
 * 
 * Implementation of hook_checkout_form().
 * 
 */
function ec_ship_checkout_form(&$form, &$form_state) {
  $txn =& $form_state['txn'];
  
  if ($txn->shippable) {
    $options = ec_ship_get_shipping_methods($txn);

    $form['ec_ship'] = array(
      '#prefix' => '<div class="ec-ship-shipping">',
      '#suffix' => '</div>',
      '#theme' => 'ec_ship_shipping',
    );
    $form['ec_ship']['title'] = array(
      '#value' => t('Shipping Method'),
    );
    $form['ec_ship']['shipping'] = array(
     '#prefix' => '<div class="shipping">',
     '#suffix' => '</div>',
     '#tree' => TRUE,
    );
    $form['ec_ship']['shipping']['buttons'] = array(
      '#prefix' => '<div class="shipping-buttons ec-inline-form">',
      '#suffix' => '</div>',
      '#weight' => -1,
    );
    $form['ec_ship']['shipping']['buttons']['select_method'] = array(
      '#title' => t('Select Your Shipping Method'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $form_state['txn']->shipping['method'],
    );
    #$form['ec_ship']['shipping']['buttons']['method_submit'] = array(
    #  '#type' => 'submit',
    #  '#value' => t('Select Method'),
    #  '#access' => count($options) > 1,
    #  '#submit' => array('ec_ship_checkout_alter_shipping'),
    #  '#ahah' => array(
    #    'path' => 'checkout/ajax/shipping',
    #    'method' => 'replace',
    #  ),
    #);
    $form['ec_ship']['#theme'] = 'ec_ship_review_form';
    $form['ec_ship']['#weight'] = 0;
  }
}

/**
 * AJAX to allow the customer to change address.
 */
function ec_ship_checkout_update_shipping_js() {
  ctools_include('object-cache');
  
  if (!($cached_form = form_get_cache($_POST['form_build_id'], $cached_form_state)) || !($txn = ctools_object_cache_get('transaction', 'ec_checkout'))) {
    form_set_error('form_token', t('Validation error, please try again. If this error persists, please contact the site administrator.'));
    $output = theme('status_messages');
    print drupal_to_js(array('status' => TRUE, 'data' => $output));
    exit();
  }

  $form_state = array('values' => $_POST);
  $form_state['txn'] =& $txn;
  $txn->shipping['method'] = $form_state['values']['shipping']['buttons']['select_method'];
  $form = array();

  $txn->shipping['price'] = ec_ship_get_shipping_cost($txn);

  ec_ship_checkout_form($form, $form_state);
  $cached_form['ec_ship'] = $form['ec_ship'];
  $new_form = array('ec_ship' => array('shipping' => array('#tree' => TRUE)));
  $new_form['ec_ship'] = $form['ec_ship'];
  $form = $new_form;
  form_set_cache($_POST['form_build_id'], $cached_form, $cached_form_state);
  
  // Render the form for output.
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
    '#tree' => FALSE,
    '#parents' => array(),
  );

  drupal_alter('form', $form, array(), 'ec_ship_checkout_form');
  $form_state = array('submitted' => FALSE);
  $form = form_builder('ec_checkout_form', $form, $form_state);
  $output = theme('status_messages') . drupal_render($form);

  ctools_object_cache_set('transaction', 'ec_checkout', $txn);
  // We send the updated file attachments form.
  // Don't call drupal_json(). ahah.js uses an iframe and
  // the header output by drupal_json() causes problems in some browsers.
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/*
 * 
 * 
 * 
 */
function ec_ship_checkout_alter_shipping(&$form, &$form_state) {
  $txn =& $form_state['txn'];
  $txn->shipping['method'] = $form_state['values']['shipping']['buttons']['select_method'];
  $txn->shipping['price'] = ec_ship_get_shipping_cost($txn);
}


/**
 * 
 * Implementation of hook_checkout_calculate().
 * 
 */
function ec_ship_checkout_update(&$form, &$form_state) {
  $txn =& $form_state['txn'];

  $weight = 0.0;
  if ($txn->shippable) {
    foreach ($txn->items as $item) {
      $weight = floatval($item->weight)+floatval($weight);
    }
    $txn->shipping['description'] = t('Shipping');
    $txn->shipping['totalweight'] = $weight;
    $txn->shipping['method'] = $form_state['values']['shipping']['buttons']['select_method'];
    $txn->shipping['price'] = ec_ship_get_shipping_cost($txn);
    $txn->shipping['provider'] = 'ec_cups';
  }
}